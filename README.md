== README

Rails App to create and serve endpoints. Take-home assignment for Babbel interview process

**Ruby version**

ruby 2.3.7p456

**Setup**

* bundle install
* rake db:create
* rake db:migrate
* rails s

**How to run the test suite**

* bundle install
* rake db:test:prepare
* rspec spec

**Creating Endpoints**

` POST /endpoint { endpoint: { DATA } } `

valid data attributes are :verb, :path, :response

* :verb must be valid http verb [GET, POST, PUT/PATCH, DELETE]
