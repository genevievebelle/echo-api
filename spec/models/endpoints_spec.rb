require 'spec_helper'

describe Endpoint do
  before { @endpoint = FactoryBot.build(:endpoint) }

  subject { @endpoint }

  it { should respond_to(:verb) }
  it { should respond_to(:path) }
  it { should respond_to(:response) }

  it { should be_valid }
end
