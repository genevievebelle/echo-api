require 'spec_helper'

describe Api::EndpointsController do
  describe "GET #index" do
    before(:each) do
      3.times { FactoryBot.create :endpoint }
      get :index, format: :json
    end

    it "returns 3 records from the database" do
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response.size).to eq(3)
    end

    it "responds with 200" do
      expect(response._status_code).to be(200)
    end
  end

  describe "GET #show" do
    context "when endpoint exists" do
      before(:each) do
        @endpoint = FactoryBot.create :endpoint
        get :show, id: @endpoint.id, format: :json
      end

      it "returns the information about the endpoint in a hash" do
        json_response = JSON.parse(response.body, symbolize_names: true)
        expect(json_response[:path]).to eql @endpoint.path
      end

      it "responds with 200" do
        expect(response._status_code).to be(200)
      end
    end

    context "when endpoint does not exist" do
      before(:each) do
        get :show, id: '0000'
      end

      it "responds with 404" do
        expect(response._status_code).to be(404)
      end
    end
  end

  describe "POST #create" do
    context "when it is successfully created" do
      before(:each) do
        @attributes = FactoryBot.attributes_for :endpoint
        post :create, { endpoint: @attributes }
      end

      it "renders the json representation for the endpoint just created" do
        json_response = JSON.parse(response.body, symbolize_names: true)
        expect(json_response[:path]).to eql @attributes[:path]
      end

      it "responds with 201" do
        expect(response._status_code).to be(201)
      end

    end

    context "when it is not created" do
      before(:each) do
        @invalid_attributes = { verb: 'GET', path: '/greeting' }
        post :create, { endpoint: @invalid_attributes }
      end

      it "renders an errors json" do
        json_response = JSON.parse(response.body, symbolize_names: true)
        expect(json_response).to have_key(:errors)
      end

      it "responds with 422" do
        expect(response._status_code).to be(422)
      end
    end
  end

  describe "PUT/PATCH #update" do
    before(:each) do
      @endpoint = FactoryBot.create :endpoint
    end

    context "when it is successfully updated" do
      before(:each) do
        patch :update, { id: @endpoint.id,
          endpoint: { path: "/new_path" } }
      end

      it "renders the json representation for the updated endpoint" do
        json_response = JSON.parse(response.body, symbolize_names: true)
        expect(json_response[:path]).to eql "/new_path"
      end

      it "responds with 200" do
        expect(response._status_code).to be(200)
      end
    end

    context "when is not updated" do
      before(:each) do
        patch :update, { id: @endpoint.id,
          endpoint: { verb: "invalid" } }
      end

      it "renders an errors json" do
        json_response = JSON.parse(response.body, symbolize_names: true)
        expect(json_response).to have_key(:errors)
      end

      it "responds with 422" do
        expect(response._status_code).to be(422)
      end
    end
  end

  describe "DELETE #destroy" do
    context "when endpoint exists" do
      before(:each) do
        @endpoint = FactoryBot.create :endpoint
        delete :destroy, { id: @endpoint.id }
      end

      it "responds with 204" do
        expect(response._status_code).to be(204)
      end
    end

    context "when endpoint does not exist" do
      before(:each) do
        delete :destroy, { id: 'not found' }
      end

      it "responds with 404" do
        expect(response._status_code).to be(404)
      end
    end
  end
end
