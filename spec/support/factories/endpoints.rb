FactoryBot.define do
  factory :endpoint do
    verb { 'GET' }
    path { 'greeting' }
    response { '{
      "code": 200,
      "headers": {},
      "body": "\"{ \"message\": \"Hello, world\" }\""
    }' }
  end
end
