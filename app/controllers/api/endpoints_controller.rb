class Api::EndpointsController < ApplicationController
  respond_to :json
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def index
    respond_with Endpoint.all
  end

  def show
    respond_with Endpoint.find(params[:id])
  end

  def create
    endpoint = Endpoint.new(endpoint_params)
    if endpoint.save
      render json: endpoint, status: 201
    else
      render json: { errors: endpoint.errors }, status: 422
    end
  end

  def update
    endpoint = Endpoint.find(params[:id])
    if endpoint.update(endpoint_params)
      render json: endpoint, status: 200
    else
      render json: { errors: endpoint.errors }, status: 422
    end
  end

  def destroy
    if endpoint = Endpoint.find(params[:id])
      endpoint.destroy
      head 204
    else
      head 404
    end
  end

  private

  def endpoint_params
    params.require(:endpoint).permit(:verb, :path, :response)
  end

  def record_not_found
    render json: "404 Not Found", status: 404
  end
end
