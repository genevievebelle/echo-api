class Endpoint < ActiveRecord::Base
  validates :verb, :path, :response, presence: true
  validates :verb, inclusion: { in: %w(GET POST PATCH PUT DELETE),
    message: "%{value} is not a valid HTTP verb" }
end
