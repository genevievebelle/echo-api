EchoApi::Application.routes.draw do
  namespace :api, defaults: { format: :json },
                              constraints: { subdomain: 'api' }, path: '/'  do
    resources :endpoints, :only => [:index, :show, :create, :update, :destroy]
  end
end
